// Lesson 1
function total(a,b,c,d,e){
    var diem = a + b + c + d +e;
    return diem;
}
function ketqua(){
    var diemChuan = document.getElementById("txt-diem-chuan").value*1;
    var kv = document.getElementById("txt-kv").value*1;
    var dt = document.getElementById("txt-dt").value*1;
    var diem1 = document.getElementById("txt-diem1").value*1;
    var diem2 = document.getElementById("txt-diem2").value*1;
    var diem3 = document.getElementById("txt-diem3").value*1;
    var result1 = document.getElementById("result1");

    var tongDiem = total(kv,dt,diem1,diem2,diem3);
    if(diem1==0 || diem2==0 || diem3==0){
        result1.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
        bạn đã rớt chúc bạn may Mắn lần sau.
       </h2>`
    }else if(diem1>10 || diem2>10 || diem3>10 || diem1<0 || diem2<0 || diem3<0){
        alert("Dữ Liệu Không Hợp Lệ");
    }else{
        if(diemChuan <= tongDiem){
            result1.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
            bạn đã đậu: tổng điểm là ${tongDiem} 
           </h2>`
        }else{
            result1.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
            bạn đã rớt: tổng điểm là ${tongDiem} 
           </h2>`
        }
    }
}
// lesson 2
function duoi50Kw(a){
    var tien= a*500 ;
    return tien;
}
function tu50Den100(a){
    var tien = 50*500 + (a-50)*650;
    return tien;
}
function tu100Den200(a){
    var tien = 50*500 + 50*650 +(a-100)*850;
    return tien;
}
function tu200Den350(a){ 
    var tien = 50*500 + 50*650 + 100*850 + (a-200)*1100;
    return tien;
}
function tu350(a){
    var tien = 50*500 + 50*650 +100*850 + 150*1100 + (a-350)*1300;
    return tien;
}
function Tinh(){
    var ten = document.getElementById("txt-ten").value;
    var kw = document.getElementById("txt-kw").value*1;
    var result2 = document.getElementById("result2");
    
    if(0<kw && kw<=50){
       var tienDien = duoi50Kw(kw);
       result2.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
       họ và tên: ${ten}; tiền điện: ${tienDien.toLocaleString()} 
       </h2>`
    }else if(50<kw && kw<=100){
        var tienDien = tu50Den100(kw);
        result2.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
       họ và tên: ${ten}; tiền điện: ${tienDien.toLocaleString()} 
       </h2>`
    }else if (100<kw && kw<=200){
        var tienDien = tu100Den200(kw);
        result2.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
       họ và tên: ${ten}; tiền điện: ${tienDien.toLocaleString()} 
       </h2>`
    }else if(200<kw && kw<=350){
        var tienDien = tu200Den350(kw);
        result2.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
       họ và tên: ${ten}; tiền điện: ${tienDien.toLocaleString()} 
       </h2>`
    }else if(350<kw){
        var tienDien = tu350(kw);
        result2.innerHTML=`<h2 class="mt-2 text-danger text-center text-capitalize">
       họ và tên: ${ten}; tiền điện: ${tienDien.toLocaleString()} 
       </h2>`
    }else{
        alert("Số Kw Không Hợp Lệ Vui Lòng Nhập Lại"); 
    }
}